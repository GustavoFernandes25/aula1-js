var text="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut suscipit condimentum justo eget volutpat. Morbi felis diam, pellentesque sed aliquam eu, aliquam vitae elit. Integer ut sapien est. Nulla at elit nec nisl dignissim viverra sit amet in libero. Nunc sed bibendum enim. Praesent pharetra semper nulla vitae interdum. Nunc interdum enim imperdiet porta bibendum. Aenean ex ipsum, placerat sed sollicitudin et, tempus non sapien. Suspendisse congue vulputate molestie. Cras auctor vehicula justo tincidunt accumsan. Pellentesque vel iaculis nibh. Aliquam scelerisque eleifend vestibulum.\n" + "\n"+ "Phasellus non dictum eros. Praesent cursus laoreet ipsum, in porta nisi hendrerit eu. Pellentesque scelerisque felis ut nunc sagittis, quis ultricies nunc euismod. Curabitur quis neque in magna efficitur luctus mollis vel odio. In eu condimentum orci. Curabitur ut ex imperdiet, consectetur diam at, vestibulum risus. Nunc pharetra, est eu placerat dapibus, risus odio blandit ex, at aliquam enim augue sit amet lacus. Cras bibendum, quam non ultrices porttitor, leo urna egestas eros, a sagittis ligula erat vitae purus. In sit amet porta turpis.\n"+"\n"+"Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae Cras mauris mi, aliquet ac dui non, pellentesque venenatis metus. Integer hendrerit tortor id pharetra ultrices. Suspendisse cursus suscipit congue. Vestibulum ornare faucibus interdum. Aliquam dapibus elit sed lorem laoreet tincidunt. Duis et sem fermentum urna tincidunt rutrum sit amet volutpat elit."

let resultado = ' ' // Variável que vai ser armazenado todo os números correspondentes as letras/caracteres do texto.


//Aqui é realizado um laço de repetição, onde ele varre todo o texto comparando as letras e armazenando o número correspondente a ela na variável resultado.
for (let i = 0; i < text.length; i++) { 
    let caractere = text[i];
    caractere = caractere.toLowerCase(); // Foi necessário fazer com que todo o texto ficasse em minúsculo para identificação de todos os caracteres do texto.
    if(caractere === "a"){ // Assim  como acontece em todos os casos seguintes, aqui é realizado a comparação do caractere correspondente a posição i(1 = l) com o caractere 'a', dando false no primeiro laço.. 
        resultado += '1';
    }
    if(caractere === "b"){
        resultado += '2';  
    }
    if(caractere === "c"){
        resultado += '3';
    }
    if(caractere === "d"){
        resultado += '4';
    }
    else if(caractere === "e"){
        resultado += '5';
    }
    else if(caractere === "f"){
        resultado += '6';
    }
    else if(caractere === "g"){
        resultado += '7';
    }
    else if(caractere === "h"){
        resultado += '8';
    }
    else if(caractere === "i"){
        resultado += '9';
    }
    else if(caractere === "j"){
        resultado += '10';
    }
    else if(caractere === "k"){
        resultado += '11';
    }
    else if(caractere === "l"){
        resultado += '12';
    }
    else if(caractere === "m"){
        resultado += '13';
    }
    else if(caractere === "n"){
        resultado += '14';
    }
    else if(caractere === "o"){
        resultado += '15';
    }
    else if(caractere === "p"){
        resultado += '16'; 
    }
    else if(caractere === "q"){
        resultado += '17';
    }
    else if(caractere === "r"){
        resultado += '18';
    }
    else if(caractere === "s"){
        resultado += '19';
    }
    else if(caractere === "t"){
        resultado += '20';
    }
    else if(caractere === "u"){
        resultado += '21';
    }
    else if(caractere === "v"){
        resultado += '22';
    }
    else if(caractere === "w"){
        resultado += '23';
    }
    else if(caractere === "x"){
        resultado += '24';
    }
    else if(caractere === "y"){
        resultado += '25';
    }
    else if(caractere === "z"){
        resultado += '26';
    }
    else if(caractere === "."){
        resultado += '0';
    }
    else if(caractere === ","){
        resultado += '26';
    }
}

console.log(resultado) // Mostra o resultado no terminal